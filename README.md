---
title: "About"
permalink: "/about/"
layout: page
---

This is the place where I share the content of my mind (part of it) with the outside world. This website serves the following purposes:

* to summarize my concepts so that I remember them and can return to them,
* to make possible for other people to benefit from these concepts.

You can see my other blogs (about other topics) here: [damc4.gitlab.io](https://damc4.gitlab.io).
