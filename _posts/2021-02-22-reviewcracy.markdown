---
layout: post
title:  "Reviewcracy - a system of government"
date:   2021-02-22 21:04:42 +0100
categories: society
comments: true
---
Reviewcracy is a system of government that aims to solve the disadvantages of the existing systems of government while retaining their advantages.

By "system of government" I mean a method/system through which a group of people makes the decisions in order to achieve some goal. The cooperating group of people can be a country, an organization, a group of friends or any other group of people.

In this article, I will refer to the people belonging to that group as "citizens".

# Goal

Reviewcracy aims to maximize the average happiness of the citizens (that doesn't mean that it ignores equality or justice because some kinds of equality or justice lead to happiness).

# System

There is a "script". Script is a set of files/documents describing all the rules in the cooperation (including the system of government itself) but also one-time decisions. All citizens are recommended to follow the script. There are penalties for not following the script.

The beginning state of the script is decided by the founder of cooperation. At any time of the cooperation, every citizen can propose a change to the script.

When a citizen proposes a change, the change is then reviewed by other citizens. Review is the process of discussing and approving/declining the change. If the change is approved, then the change to the script is made.

The advantage of everyone being able to propose a change (instead of only some group of lawmakers) is that there will be more good propositions. There will be also more bad propositions, but given that the system for approving/declining those changes is good, the benefit from having more good propositions is higher than the cost of having more bad propositions. This is because a good change will most likely be approved and bad change will most likely be declined.

Review of a change consists of two parts that take place at the same time: discussion and voting.

The purpose of discussion is to exchange the knowledge regarding the consequences of the change.

Ideally, if the citizens exchange they knowledge, they should arrive to the same conclusion, but that won't always be the case. If the change is approved or not is decided by voting in which every citizen can participate. However, standard voting procedure is a terrible way to make decisions. For that reason, it's not decided by standard voting procedure but improved voting procedure which solves the problems of the standard voting.

# The end

I described the idea very very briefly. There is much more depth/detail to that idea, but I want to see if there is initial interest before I spend lots of time writing about it. For that reason, I'm ending it here, but I'm happy to go deeper in the comments. Here are some questions that I'd be happy to answer:
1. Describe in detail the improved voting procedure. How does it work and why?
2. Does Reviewcracy work and if it does, then what is the proof?
3. In that system, the decisions are directly taken by the citizens, there are no elected governors, is this a good idea?

# Update

There was initial interest, so I decided to describe the idea in greater detail. I was writing a GitBook describing the idea and during the time when I was describing it, I came up with a better system. For that reason, I decided to put the Reviewcracy project on hold. I will later describe the new system instead.
